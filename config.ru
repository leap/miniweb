ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../Gemfile', __FILE__)
require 'bundler/setup' # Set up gems listed in the Gemfile.

$LOAD_PATH << File.expand_path('../lib', __FILE__)

require_relative 'config/app_config'
require 'miniweb'
run Miniweb.new
