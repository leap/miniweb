require 'yaml'

APP_ENV = ENV['APP_ENV'] || 'production'
APP_CONFIG = ["defaults.yml", "config.yml"].inject({}) do |config, file|
  filepath = File.expand_path(file, File.dirname(__FILE__))
  if File.exist?(filepath) && settings = YAML.load_file(filepath)[APP_ENV]
    # convert all keys to symbols in a ruby 2.3 compatible way.
    # In ruby 2.5 we can use transform_keys
    settings = settings.inject({}) do |memo,(k,v)|
      memo.merge({k.to_sym => v})
    end
    config.merge(settings)
  else
    config
  end
end


