require 'rack/test'
require 'minitest/autorun'

OUTER_APP = Rack::Builder.parse_file("config.ru").first

class AppTest < Minitest::Test
  include Rack::Test::Methods

  def app
    OUTER_APP
  end
end
