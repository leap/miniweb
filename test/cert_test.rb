require 'test_helper'

class FirstTest < AppTest

  def test_wrong_url
    get 'bla'
    assert_equal 404, last_response.status
  end

  def test_getting_cert
    get '1/cert'
    assert_equal 200, last_response.status
  end

  def test_getting_cert_version_2
    get '2/cert'
    assert_equal 200, last_response.status
  end

  def test_creating_cert
    post '1/cert'
    assert_equal 200, last_response.status
  end

  def test_cert_content
    post '2/cert'
    assert_includes last_response.body, "BEGIN RSA PRIVATE KEY"
    assert_includes last_response.body, "END RSA PRIVATE KEY"
    assert_includes last_response.body, "BEGIN CERTIFICATE"
    assert_includes last_response.body, "END CERTIFICATE"
  end
end
