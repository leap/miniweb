require 'client_certificate'

class Miniweb
  def call(env)
    handle_request env["REQUEST_METHOD"], env["PATH_INFO"]
  end

  private

  def handle_request(method, path)
    if ["POST", "GET"].include?(method) && path =~ /^\/[12]\/cert$/
      send_cert
    else
      not_found
    end
  end

  def send_cert
    cert = ClientCertificate.new(prefix: APP_CONFIG[:unlimited_cert_prefix])
    [200, { "Content-Type" => "text/plain" }, [cert.to_s]]
  end

  def not_found
    [404, {}, [""]]
  end
end
